output "dev-vpc-id" {
  value = "aws_vpc.development-vpc.id"
}

output "dev-subnet-id" {
  value = "aws_subnet.dev-subnet-1.id"
}

output "ec2_public_ip" {
  value = aws_instance.myapp-server.public_ip
}

output "aws_ami" {
  value = data.aws_ami.latest-aws-linux-server
}