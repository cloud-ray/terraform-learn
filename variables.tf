variable "aws_region" {
  description = "aws region of project"
}

variable "avail_zone" {
  description = "avail_zone of subnet 1"
}

variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "ami_id" {}
variable "instance_type" {}
variable "public_key_location" {}